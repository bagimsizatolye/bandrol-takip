<p align="center"><img src="https://bandroltakip.ofissizler.org/img/ofissizler-logo.png" height="150"> &nbsp; &nbsp; <img src="https://bagimsizatolye.org/img/logo-dark-white-bg.png" height="150"></p>

## Bandrol Takip Sistemi

Yazarlar ve çevirmenler, telif karşılığı çalıştıkları kitapların yeni baskıları çıktığında haberdar olmalı. Kültür ve Turizm Bakanlığı'nın yaptığı [sistem](http://www.telifhaklari.gov.tr/kitap-bandroller) oldukça kötü çalışıyor. Sadece yedi günlük liste çıkarıyor ve insanların burada araması bekleniyor. Ayrıca yazarın/çevirmenin düzenli olarak buraya girip bakması gerekiyor.

Bandrol takip sistemi bu veriyi daha kolay incelenebilir halde gösterme amacıyla yapıldı. Ayrıca yazarların ve çevirmenlerin düzenli olarak bu listeye bakması yerine, bildirim mekanizması ile günlük olarak kişileri uyarır.

[Bandrol takip sistemine giriş için tıklayın](https://bandroltakip.ofissizler.org)

## [Ofissizler](https://ofissizler.org)

[Ofissizler](https://ofissizler.org), freelance çalışanların haklarını savunduğu bir dayanışma ağıdır. Ofissizler hakkında detaylı bilgi için [tıklayın](https://ofissizler.org/kimdir).

## [Bağımsız Atölye](https://bagimsizatolye.org)

Bağımsız Atölye, birlikte çalışmanın yatay ve adil bir biçimidir. Bu sistemi üretenlerdir. Detaylı bilgi için [tıklayın](https://bagimsizatolye.org/manifesto).

## Destek

Bandrol takip sistemi özgür bir yazılımdır. Yani eğer biraz yazılım işlerinden anlıyorsanız siz de programlama kısmına destek olabilirsiniz. Başka tür destekler ve önerileriniz için e-posta gönderebilirsiniz: <ege@bagimsizatolye.org>.
