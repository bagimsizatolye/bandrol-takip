<?php

use Carbon\Carbon;

function ba_format_date_fe(Carbon $carbon, $format = 'D MMMM YYYY'): string
{
    return $carbon->locale(app()->getLocale())->isoFormat($format);
}
