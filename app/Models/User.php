<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'receives_email_notifications',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'receives_email_notifications' => 'boolean',
    ];

    public function writers()
    {
        return $this->hasMany(Writer::class);
    }

    public function subscribedWriters()
    {
        return $this->belongsToMany(Writer::class, 'user_subscriptions', 'user_id', 'writer_id');
    }

    public function subscribedBooks()
    {
        return $this->belongsToMany(Book::class, 'user_subscriptions', 'user_id', 'book_id');
    }

    public function subscribedPublishers()
    {
        return $this->belongsToMany(Publisher::class, 'user_subscriptions', 'user_id', 'publisher_id');
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }
}
