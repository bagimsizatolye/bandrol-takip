<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookPrint extends Model
{
    protected $fillable = [
        'book_id',
        'print_run',
        'printed_on',
    ];

    public $casts = [
        'printed_on' => 'date',
    ];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
