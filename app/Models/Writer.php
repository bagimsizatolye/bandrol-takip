<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Writer extends Model
{
    const sAuthor = 1;
    const sTranslator = 2;

    protected $fillable = [
        'name',
        'is_author',
        'is_translator',
        'user_id',
    ];

    public $casts = [
        'is_author' => 'boolean',
        'is_translator' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function booksAuthored()
    {
        return $this->belongsToMany(Book::class, 'book_writers', 'writer_id', 'book_id')
            ->wherePivot('writer_status', Writer::sAuthor);
    }

    public function booksTranslated()
    {
        return $this->belongsToMany(Book::class, 'book_writers', 'writer_id', 'book_id')
            ->wherePivot('writer_status', Writer::sTranslator);
    }

    public function books()
    {
        return $this->belongsToMany(Writer::class, 'book_writers', 'writer_id', 'book_id');
    }

    public function subscribedUsers()
    {
        return $this->belongsToMany(
            User::class,
            'user_subscriptions',
            'writer_id',
            'user_id'
        );
    }
}
