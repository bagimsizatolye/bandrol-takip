<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = [
        'name',
        'title',
    ];

    public function books()
    {
        return $this->hasMany(Book::class, 'publisher_id')
            ->orderBy('title');
    }

    public function subscribedUsers()
    {
        return $this->belongsToMany(
            User::class,
            'user_subscriptions',
            'publisher_id',
            'user_id'
        );
    }
}
