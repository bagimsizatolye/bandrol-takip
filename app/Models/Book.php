<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title',
        'publisher_id',
        'book_type_id',
    ];

    public function publisher()
    {
        return $this->belongsTo(Publisher::class, 'publisher_id');
    }

    public function bookType()
    {
        return $this->belongsTo(BookType::class, 'book_type_id');
    }

    public function authors()
    {
        return $this->belongsToMany(Writer::class, 'book_writers', 'book_id', 'writer_id')
            ->wherePivot('writer_status', Writer::sAuthor);
    }

    public function translators()
    {
        return $this->belongsToMany(Writer::class, 'book_writers', 'book_id', 'writer_id')
            ->wherePivot('writer_status', Writer::sTranslator);
    }

    public function writers()
    {
        return $this->belongsToMany(Writer::class, 'book_writers', 'book_id', 'writer_id');
    }

    public function bookPrints()
    {
        return $this->hasMany(BookPrint::class, 'book_id')
            ->orderBy('print_run', 'desc');
    }

    public function subscribedUsers()
    {
        return $this->belongsToMany(
            User::class,
            'user_subscriptions',
            'book_id',
            'user_id'
        );
    }
}
