<?php

namespace App\Support;

class MetaData
{
    public $seo;

    public function __construct(array $metaData = [])
    {
        $this->seo = new \stdClass();
        $this->seo->abstract = '';
        $this->seo->description = 'Yayınevleri sizi yeni baskılardan haberdar etmiyor mu? Bandrol alımlarını kendiniz takip edin, yeni baskı yapıldığında bildirim alın.';
        $this->seo->keywords = 'ofissizler, kitap, bandrol, telif, bandrol takip, çevirmen, yazar, yayınevi';
        $this->seo->image = asset('img/ofissizler-logo.png');
        $this->seo->newsKeywords = '';
        $this->seo->title = config('app.name');
        $this->seo->url = url()->current();

        foreach ($metaData as $key => $value) {
            if (empty($value)) {
                continue;
            }
            $this->seo->$key = $value;
        }

        if ($this->seo->title !== config('app.name')) {
            $this->seo->title = $this->seo->title .  ' - ' . config('app.name');
        }

        if (\strpos($this->seo->image, 'http') === false) {
            $this->seo->image = url($this->seo->image);
        }

        if ($this->seo->image === asset('img/ofissizler-logo.png')) {
            $this->seo->imageWidth = 1080;
            $this->seo->imageHeight = 512;
        }
    }
}
