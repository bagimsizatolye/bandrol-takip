<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;

use Carbon\Carbon;

use App\Models\Book;
use App\Models\BookPrint;
use App\Models\BookType;
use App\Models\Publisher;
use App\Models\Writer;
use App\Services\BookService;

class FetchPrints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ba:fetch-prints
        {--all : If exists, retrieves all records for current year }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var BookService
     */
    protected $bookService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BookService $bookService)
    {
        parent::__construct();

        $this->bookService = $bookService;
    }

    public function handle(): int
    {
        $retrieveAll = $this->option('all');

        if ($retrieveAll) {
            // For retrieving whole data
            $date = Carbon::createFromFormat('Y-m-d', date('Y') . '-01-01');

            while ($date < now()) {
                $this->info('Processing for date: ' . $date->format('Y-m-d'));
                $this->handleForDate($date);
                $date->add('day', 7);
            }
        }

        return $this->handleForDate();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handleForDate(Carbon $date = null)
    {
        if (is_null($date)) {
            $date = now()->sub(5, 'days');
        }

        $html = file_get_contents('https://www.telifhaklari.gov.tr/kitap-bandroller?date=' . $date->format('d.m.Y'));

        $html = str_replace(
            [
                '<header',
                '</header>',
                '<footer',
                '</footer>',
                '<nav',
                '</nav>',
                '<section',
                '</section>',
                '<article',
                '</article>',
                '&',
            ],
            [
                '<div',
                '</div>',
                '<div',
                '</div>',
                '<div',
                '</div>',
                '<div',
                '</div>',
                '<div',
                '</div>',
                '&nbsp;',
            ],
            $html
        );
        $dom = new \domDocument;

        $dom->loadHTML($html);
        $dom->preserveWhiteSpace = false;
        $tables = $dom->getElementsByTagName('table')->item(0)->getElementsByTagName('tbody');

        $rows = $tables->item(0)->getElementsByTagName('tr');

        foreach ($rows as $row) {
            $cols = $row->getElementsByTagName('td');

            $publisherText = $this->getTextTitle($cols->item(5)->nodeValue);
            $publisher = Publisher::firstOrCreate([
                'title' => $publisherText,
            ]);

            $type = $this->getTextTitle($cols->item(1)->nodeValue);
            $bookType = BookType::firstOrCreate([
                'name' => $type,
            ]);

            $bookTitle = $this->getTextTitle($cols->item(0)->nodeValue);
            $book = Book::firstOrCreate([
                'title' => $bookTitle,
                'book_type_id' => $bookType->id,
                'publisher_id' => $publisher->id,
            ]);

            $authors = $this->getAuthors($cols->item(2)->nodeValue)->pluck('id');
            $translators = $this->getTranslators($cols->item(3)->nodeValue)->pluck('id');

            $writersSync = [];
            foreach ($authors as $item) {
                $writersSync[$item] = ['writer_status' => Writer::sAuthor];
            }
            foreach ($translators as $item) {
                $writersSync[$item] = ['writer_status' => Writer::sTranslator];
            }
            $book->writers()->sync($writersSync);

            $printRunDate = Carbon::createFromFormat('d.m.Y', $cols->item(6)->nodeValue);
            $printRun = (int)$cols->item(4)->nodeValue;

            $bookPrint = BookPrint::where([
                'book_id' => $book->id,
                'print_run' => $printRun,
                'printed_on' => $printRunDate->format('Y-m-d'),
            ])->first();

            if (!$bookPrint) {
                $bookPrint = BookPrint::create([
                    'book_id' => $book->id,
                    'print_run' => $printRun,
                    'printed_on' => $printRunDate->format('Y-m-d'),
                ]);

                $this->bookService->notifyForBookPrint($bookPrint);
            }
        }

        return 0;
    }

    protected function getAuthors(string $authorNames): Collection
    {
        $authorNames = explode(',', $authorNames);
        $writers = collect();

        foreach ($authorNames as $item) {
            $item = trim($item);
            $item = $this->getTextTitle($item);
            $writer = Writer::firstOrCreate([
                'name' => $item,
            ]);
            if (!$writer->is_author) {
                $writer->is_author = true;
                $writer->save();
            }
            $writers->push($writer);
        }

        return $writers;
    }

    protected function getTranslators(string $authorNames): Collection
    {
        $authorNames = explode(',', $authorNames);
        $writers = collect();

        foreach ($authorNames as $item) {
            $item = trim($item);
            $item = $this->getTextTitle($item);
            if (empty($item)) {
                continue;
            }
            $writer = Writer::firstOrCreate([
                'name' => $item,
            ]);
            if (!$writer->is_translator) {
                $writer->is_translator = true;
                $writer->save();
            }
            $writers->push($writer);
        }

        return $writers;
    }

    protected function getTextTitle(string $text): string
    {
        // return mb_convert_case(mb_strtolower($text), MB_CASE_TITLE);

        $text = str_replace(
            ['I', 'İ',],
            ['ı', 'i',],
            $text
        );

        $text = mb_strtolower($text);

        $text = str_replace(
            [' ı', ' i',],
            [' I', ' İ',],
            $text
        );

        if (mb_strpos($text, 'ı') === 0) {
            $text = 'I' . mb_substr($text, 1);
        }

        if (mb_strpos($text, 'i') === 0) {
            $text = 'İ' . mb_substr($text, 1);
        }

        return mb_convert_case($text, MB_CASE_TITLE);
    }
}
