<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Writer;
use App\Services\BookService;

use App\Support\MetaData;

class WriterController extends Controller
{
    /**
     * @var BookService
     */
    protected $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Writer  $writer
     * @return \Illuminate\Http\Response
     */
    public function show(Writer $writer)
    {
        $writer->load([
            'booksAuthored',
            'booksTranslated',
        ]);

        $user = auth()->user();

        $subscribed = false;

        if ($user) {
            $subscribed = $user->subscribedWriters()->wherePivot('writer_id', $writer->id)->first() ? true : false;
        }

        $meta = new MetaData([
            'title' => $writer->name,
        ]);

        return view('writer.show', [
            'writer' => $writer,
            'subscribed' => $subscribed,
            'meta' => $meta,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Writer  $writer
     * @return \Illuminate\Http\Response
     */
    public function edit(Writer $writer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Writer  $writer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Writer $writer)
    {
        if (!auth()->check()) {
            return redirect()->route('register');
        }

        $formData = $request->validate([
            'subscribe' => 'in:0,1',
        ]);

        if (array_key_exists('subscribe', $formData)) {
            $this->bookService->setSubscribeWriter(
                auth()->user(),
                $writer,
                (boolean)$formData['subscribe']
            );
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Writer  $writer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Writer $writer)
    {
        //
    }
}
