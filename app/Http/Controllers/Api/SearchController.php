<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Services\BookService;

class SearchController extends Controller
{
    /**
     * @var BookService
     */
    protected $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getWriterList(Request $request)
    {
        $filterData = $request->validate([
            'name' => 'sometimes|string|nullable|min:2',
            'writer_status' => 'sometimes|nullable',
        ]);

        $list = $this->bookService->getWriterList($filterData, 0, 100);

        return response()->json($list);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getBookList(Request $request)
    {
        $filterData = $request->validate([
            'title' => 'sometimes|string|nullable|min:2',
        ]);

        $list = $this->bookService->getBookList($filterData, 0, 100);

        return response()->json($list);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getPublisherList(Request $request)
    {
        $filterData = $request->validate([
            'title' => 'sometimes|string|nullable|min:2',
        ]);

        $list = $this->bookService->getPublisherList($filterData, 0, 100);

        return response()->json($list);
    }
}
