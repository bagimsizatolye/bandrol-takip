<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Book;
use App\Services\BookService;

use App\Support\MetaData;

class BookController extends Controller
{
    /**
     * @var BookService
     */
    protected $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $book->load([
            'authors',
            'translators',
            'publisher',
            'bookPrints',
        ]);

        $user = auth()->user();

        $subscribed = false;

        if ($user) {
            $subscribed = $user->subscribedBooks()->wherePivot('book_id', $book->id)->first() ? true : false;
        }

        $meta = new MetaData([
            'title' => $book->title,
        ]);

        return view('book.show', [
            'book' => $book,
            'subscribed' => $subscribed,
            'meta' => $meta,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        if (!auth()->check()) {
            return redirect()->route('register');
        }

        $formData = $request->validate([
            'subscribe' => 'in:0,1',
        ]);

        if (array_key_exists('subscribe', $formData)) {
            $this->bookService->setSubscribeBook(
                auth()->user(),
                $book,
                (boolean)$formData['subscribe']
            );
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
