<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Publisher;
use App\Services\BookService;

use App\Support\MetaData;

class PublisherController extends Controller
{
    /**
     * @var BookService
     */
    protected $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function show(Publisher $publisher)
    {
        $publisher->load([
            'books',
        ]);

        $bookPrintList = $this->bookService->getPublisherBookPrintList($publisher);

        $user = auth()->user();

        $subscribed = false;

        if ($user) {
            $subscribed = $user->subscribedPublishers()->wherePivot('publisher_id', $publisher->id)->first() ? true : false;
        }

        $meta = new MetaData([
            'title' => $publisher->title,
        ]);

        return view('publisher.show', [
            'publisher' => $publisher,
            'bookPrintList' => $bookPrintList,
            'subscribed' => $subscribed,
            'meta' => $meta,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function edit(Publisher $publisher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publisher $publisher)
    {
        if (!auth()->check()) {
            return redirect()->route('register');
        }

        $formData = $request->validate([
            'subscribe' => 'in:0,1',
        ]);

        if (array_key_exists('subscribe', $formData)) {
            $this->bookService->setSubscribePublisher(
                auth()->user(),
                $publisher,
                (boolean)$formData['subscribe']
            );
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publisher $publisher)
    {
        //
    }
}
