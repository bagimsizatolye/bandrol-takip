<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Writer;
use App\Services\BookService;
use App\Support\MetaData;

class HomeController extends Controller
{
    /**
     * @var BookService
     */
    protected $bookService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getHome(Request $request)
    {
        return view('home');
    }

    /**
     * Show the book print index
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bookPrintIndex(Request $request)
    {
        $filters = $request->validate([
            'writer_id' => 'sometimes|nullable|int',
            'book_id' => 'sometimes|nullable|int',
            'publisher_id' => 'sometimes|nullable|int',
            'printed_on_start' => 'sometimes|nullable|date_format:Y-m-d',
            'printed_on_end' => 'sometimes|nullable|date_format:Y-m-d',
        ]);

        foreach ($filters as $key => $value) {
            if (empty($value)) {
                unset($filters[$key]);
            }
        }

        $writer = null;
        if (\array_key_exists('writer_id', $filters)) {
            $writer = $this->bookService->getWriter($filters['writer_id']);
        }

        $book = null;
        if (\array_key_exists('book_id', $filters)) {
            $book = $this->bookService->getBook($filters['book_id']);
        }

        $publisher = null;
        if (\array_key_exists('publisher_id', $filters)) {
            $publisher = $this->bookService->getPublisher($filters['publisher_id']);
        }

        if (empty($filters)) {
            $list = null;
        } else {
            $list = $this->bookService->getBookPrintList($filters, 100)->appends($request->except('page'));
        }

        $meta = new MetaData([
            'title' => 'Bandrol Ara',
        ]);

        return view('book-print.index', [
            'writerList' => [],
            'list' => $list,
            'writer' => $writer,
            'book' => $book,
            'publisher' => $publisher,
            'meta' => $meta,
        ]);
    }
}
