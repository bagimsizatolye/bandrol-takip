<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\BookPrint;

class NewBookPrintFromPublisher extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var BookPrint
     */
    protected $bookPrint;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(BookPrint $bookPrint)
    {
        $this->bookPrint = $bookPrint;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'database',
            'mail',
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $book = $this->bookPrint->book;
        $publisher = $this->bookPrint->book->publisher;

        return (new MailMessage)
            ->subject(__('Takip ettiğin :title yayınevi yeni bandrol aldı', [
                'title' => $publisher->title,
            ]))
            ->markdown('mail.book-print.from-publisher', [
                'book' => $book,
                'publisher' => $publisher,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
