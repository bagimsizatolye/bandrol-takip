<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\BookPrint;
use App\Models\Writer;

class NewBookPrintFromWriter extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var BookPrint
     */
    protected $bookPrint;

    /**
     * @var Writer
     */
    protected $writer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(BookPrint $bookPrint, Writer $writer)
    {
        $this->bookPrint = $bookPrint;
        $this->writer = $writer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'database',
            'mail',
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $book = $this->bookPrint->book;
        $publisher = $this->bookPrint->book->publisher;

        return (new MailMessage)
            ->subject(__('Takip ettiğiniz :name kişisine ait yeni bandrol alındı', [
                'name' => $this->writer->name,
            ]))
            ->markdown('mail.book-print.from-writer', [
                'book' => $book,
                'bookPrint' => $this->bookPrint,
                'publisher' => $publisher,
                'writer' => $this->writer,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'book_print_id' => $this->bookPrint->id,
            'writer_id' => $this->writer->id,
        ];
    }
}
