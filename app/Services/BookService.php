<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Book;
use App\Models\BookPrint;
use App\Models\BookType;
use App\Models\Publisher;
use App\Models\User;
use App\Models\Writer;
use App\Notifications\NewBookPrintFromBook;
use App\Notifications\NewBookPrintFromPublisher;
use App\Notifications\NewBookPrintFromWriter;

class BookService
{
    public function getBook(int $id): ?Book
    {
        return Book::find($id);
    }

    public function getWriter(int $id): ?Writer
    {
        return Writer::find($id);
    }

    public function getPublisher(int $id): ?Publisher
    {
        return Publisher::find($id);
    }

    public function getBookPrintList(
        array $filters,
        $pageLength = 30
    ): LengthAwarePaginator {
        $query = BookPrint::with([
                'book',
                'book.authors',
                'book.translators',
                'book.publisher',
                'book.bookType',
            ])
            ->select([
                'book_prints.*',
            ])
            ->join('books AS b', 'b.id', '=', 'book_prints.book_id')
            ->leftJoin('book_writers AS bw', 'b.id', '=', 'bw.book_id')
            ->groupBy('book_prints.id')
            ->orderBy('book_prints.printed_on', 'desc')
            ->orderBy('b.title', 'asc')
            ->orderBy('book_prints.print_run', 'desc');

        if (array_key_exists('author_id', $filters) and !empty($filters['author_id'])) {
            $query->where('bw.writer_id', (int)$filters['author_id'])
                ->where('bw.writer_status', Writer::sAuthor);
        }

        if (array_key_exists('translator_id', $filters) and !empty($filters['translator_id'])) {
            $query->where('bw.writer_id', (int)$filters['translator_id'])
                ->where('bw.writer_status', Writer::sTranslator);
        }

        if (array_key_exists('writer_id', $filters) and !empty($filters['writer_id'])) {
            $query->where('bw.writer_id', (int)$filters['writer_id']);
        }

        if (array_key_exists('book_id', $filters) and !empty($filters['book_id'])) {
            $query->where('book_prints.book_id', (int)$filters['book_id']);
        }

        if (array_key_exists('publisher_id', $filters) and !empty($filters['publisher_id'])) {
            $query->where('b.publisher_id', (int)$filters['publisher_id']);
        }

        if (array_key_exists('printed_on_start', $filters) and !empty($filters['printed_on_start'])) {
            $query->where('book_prints.printed_on', '>=', $filters['printed_on_start']);
        }

        if (array_key_exists('printed_on_end', $filters) and !empty($filters['printed_on_end'])) {
            $query->where('book_prints.printed_on', '<=', $filters['printed_on_end']);
        }

        return $query->paginate($pageLength);
    }

    public function getWriterList(
        array $filters = [],
        $offset = 0,
        $limit = 30
    ): Collection {
        $query = Writer::query();

        if (\array_key_exists('name', $filters) and !empty($filters['name'])) {
            $query->where('name', 'like', '%' . $filters['name'] . '%');
        }

        if (\array_key_exists('writer_status', $filters) and !empty($filters['writer_status'])) {
            if ($filters['writer_status'] === Writer::sAuthor) {
                $query->where('is_author', true);
            } elseif ($filters['writer_status'] === Writer::sTranslator) {
                $query->where('is_translator', true);
            }
        }

        return $query->offset($offset)->limit($limit)->get();
    }

    public function getBookList(
        array $filters = [],
        $offset = 0,
        $limit = 30
    ): Collection {
        $query = Book::query();

        if (\array_key_exists('title', $filters) and !empty($filters['title'])) {
            $query->where('title', 'like', '%' . $filters['title'] . '%');
        }

        return $query->offset($offset)->limit($limit)->get();
    }

    public function getPublisherList(
        array $filters = [],
        $offset = 0,
        $limit = 30
    ): Collection {
        $query = Publisher::query();

        if (\array_key_exists('title', $filters) and !empty($filters['title'])) {
            $query->where('title', 'like', '%' . $filters['title'] . '%');
        }

        return $query->offset($offset)->limit($limit)->get();
    }

    public function getPublisherBookPrintList(Publisher $publisher): Collection
    {
        return BookPrint::join('books AS b', 'b.id', '=', 'book_prints.book_id')
            ->join('publishers as p', 'p.id', '=', 'b.publisher_id')
            ->select([
                'book_prints.*',
                'b.title AS book_title',
            ])
            ->where('b.publisher_id', $publisher->id)
            ->orderBy('book_prints.printed_on', 'desc')
            ->orderBy('book_prints.print_run', 'desc')
            ->get();
    }

    public function setSubscribeWriter(
        User $user,
        Writer $writer,
        bool $subscribe
    ) {
        if ($subscribe) {
            $user->subscribedWriters()->attach($writer);
        } else {
            $user->subscribedWriters()->detach($writer);
        }
    }

    public function setSubscribeBook(
        User $user,
        Book $book,
        bool $subscribe
    ) {
        if ($subscribe) {
            $user->subscribedBooks()->attach($book);
        } else {
            $user->subscribedBooks()->detach($book);
        }
    }

    public function setSubscribePublisher(
        User $user,
        Publisher $publisher,
        bool $subscribe
    ) {
        if ($subscribe) {
            $user->subscribedPublishers()->attach($publisher);
        } else {
            $user->subscribedPublishers()->detach($publisher);
        }
    }

    public function notifyForBookPrint(BookPrint $bookPrint): array
    {
        $usersNotified = [];

        foreach ($bookPrint->book->publisher->subscribedUsers as $user) {
            $user->notify(new NewBookPrintFromPublisher($bookPrint));
            $usersNotified[] = $user->id;
        }

        foreach ($bookPrint->book->subscribedUsers as $user) {
            if (in_array($user->id, $usersNotified) === false) {
                $user->notify(new NewBookPrintFromBook($bookPrint));
                $usersNotified[] = $user->id;
            }
        }

        foreach ($bookPrint->book->authors as $writer) {
            foreach ($writer->subscribedUsers as $user) {
                if (in_array($user->id, $usersNotified) === false) {
                    $user->notify(
                        new NewBookPrintFromWriter($bookPrint, $writer)
                    );
                    $usersNotified[] = $user->id;
                }
            }
        }

        foreach ($bookPrint->book->translators as $writer) {
            foreach ($writer->subscribedUsers as $user) {
                if (in_array($user->id, $usersNotified) === false) {
                    $user->notify(
                        new NewBookPrintFromWriter($bookPrint, $writer)
                    );
                    $usersNotified[] = $user->id;
                }
            }
        }

        return $usersNotified;
    }
}
