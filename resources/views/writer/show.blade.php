@extends('layouts.app')

@section('content')
<div class="container" id="author">
  <div class="row mb-3">
    <div class="col-12 col-md-6 text-center text-md-left">
      <h3>{{ $writer->name }}</h3>
      <p>
        @if($writer->is_author)
          Yazar
        @endif
        @if($writer->is_translator)
          Çevirmen
        @endif
      </p>
    </div>

    <div class="col-12 col-md-6 text-center text-md-right">
      <form
        method="post"
        action="{{ route('writer.update', $writer->id) }}"
        @if(!$subscribed)
          data-confirm="{{ __('Bu yazara/çevirmene ait her kitap çıktığınızda e-posta alacaksınız. Emin misiniz?') }}"
        @endif
      >
        @csrf
        <input type="hidden" name="writer_id" value="{{ $writer->id }}">
        <input type="hidden" name="_method" value="put">
        @if(!$subscribed)
          <input type="hidden" name="subscribe" value="1">
          <button type="submit" class="btn btn-outline-success">Bildirimleri Aç</button>
        @else
          <input type="hidden" name="subscribe" value="0">
          <button type="submit" class="btn btn-success">Bildirimleri Kapat</button>
        @endif
      </form>
    </div>
  </div>

  @if($writer->booksAuthored->count())
    <h4 class="mb-3">Yazdığı kitaplar</h4>
    <div class="row">
      @foreach($writer->booksAuthored as $book)
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <a class="book-item" href="{{ route('book.show', $book->id) }}">
            <h5>{{ $book->title }}</h5>
            <p>
              {{ $book->publisher->title }}
            </p>
          </a>
        </div>
      @endforeach
    </div>
  @endif

  @if($writer->booksTranslated->count())
    <h4 class="mb-3">Çevirdiği kitaplar</h4>
    <div class="row">
      @foreach($writer->booksTranslated as $book)
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <a class="book-item" href="{{ route('book.show', $book->id) }}">
            <h5>{{ $book->title }}</h5>
            <p>
              {{ $book->publisher->title }}
            </p>
          </a>
        </div>
      @endforeach
    </div>
  @endif
</div>
@endsection
