@extends('layouts.app')

@section('content')
  <div class="jumbotron jumbotron-fluid" style="margin-top: -1.5rem;">
    <div class="container">
      <h1 class="display-4">Telifli eserlerinizin bandrollerini takip edin</h1>
      <p class="lead">Yayınevleri sizi yeni baskılardan haberdar etmiyor mu? Bandrol alımlarını kendiniz takip edin, yeni baskı yapıldığında bildirim alın.</p>
      <hr class="my-4">
      <p>Bandrol takip sistemiyle herhangi bir yazarı, çevirmeni, yayınevini veya kitabı takip edebilirsiniz. Her yeni baskıda e-postayla haberdar olursunuz.</p>
      <a
        class="btn btn-primary btn-lg"
        href="{{ route('book-print.index') }}"
        role="button"
      >Listeye Bak</a>
    </div>
  </div>
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-12 col-md-5 mb-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">
              Bandrol Takip Kolaylığı
            </h5>
            <div class="card-text">
              <p>
                Telif Hakları Genel Müdürlüğü'nün <a href="http://www.telifhaklari.gov.tr/kitap-bandroller" target="_blank">hazırladığı sistemde</a> yeni baskıları takip etmek hem zor hem de düzenli kontrol gerektiriyor. Bandrol takip sistemiyle bandrol alımlarından kolayca haberdar olabilirsiniz.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-5 mb-4">
        <div class="card" style="height: 100%;">
          <div class="card-body">
            <h5 class="card-title">
              Bildirim Alın
            </h5>
            <div class="card-text">
              <p>
                Bandrol takip sistemine <a href="{{ route('register') }}">kayıt olarak</a> yeni baskı çıktığında e-posta bildirimi alabilirsiniz. Bu sayede telifli eseriniz için bandrol alındığından 24 saat içinde haberdar olabilirsiniz.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-5 mb-4">
        <div class="card" style="height: 100%;">
          <div class="card-body">
            <h5 class="card-title">
              Ofissizler
            </h5>
            <div class="card-text">
              <p>
                <a href="https://ofissizler.org/" target="_blank">Ofissizler</a>, freelance çalışanların haklarını savunduğu bir dayanışma ağıdır. Ofissizler hakkında detaylı bilgi için <a href="https://ofissizler.org/kimdir/" target="_blank">tıklayın</a>.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-5 mb-4">
        <div class="card" style="height: 100%;">
          <div class="card-body">
            <h5 class="card-title">
              Özgür Yazılım
            </h5>
            <div class="card-text">
              <p>
                Bandrol takip sistemi bir özgür yazılımdır. Kodlarını incelemek, önerilerde bulunmak veya geliştirmek için <a href="https://gitlab.com/bagimsizatolye/bandrol-takip" target="_blank">GitLab</a> hesabımızı inceleyebilirsiniz.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
