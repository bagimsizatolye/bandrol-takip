@extends('layouts.app')

@section('content')
<div class="container" id="bookPrintIndex">
  <div class="card mb-4">
    <div class="card-body">
      <h5 class="card-title">
        Bandrol Listesi
      </h5>
      <div class="card-text mb-5">
        <p>
          Aşağıdaki filtrelerden birini veya birden fazlasını seçerek listeye erişebilirsiniz. <strong>Liste 1 Ocak 2019'dan günümüze kadar yayınlanmış verileri içerir.</strong> Günlük olarak güncellenir.
        </p>
        <p>
          Listedeki yazara, çevirmene, kitaba veya yayıncıya tıklayarak detaylı bilgilerini görebilir ve bildirim alabilirsiniz.
        </p>
      </div>
  <form action="{{ route('book-print.index') }}" method="get" class="mb-3">
    <div class="form-group row">
      <label class="col-sm-4 col-lg-3 col-form-label" for="writer_id">
        {{ __('Yazar veya Çevirmen') }}
      </label>
      <div class="col-sm-8 col-md-6 col-lg-3">
        <select
          class="form-control input-sm"
          name="writer_id"
          id="writer_id"
          data-placeholder="{{ __('Kişi seçin') }}"
        >
          @if(!empty($writer))
            <option value="{{ $writer->id }}" selected>
              {{ $writer->name }}
            </option>
          @endif
        </select>
      </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-4 col-lg-3 col-form-label" for="book_id">
        {{ __('Kitap') }}
      </label>
      <div class="col-sm-8 col-md-6 col-lg-3">
        <select
          class="form-control input-sm"
          name="book_id"
          id="book_id"
          data-placeholder="{{ __('Kitap seçin') }}"
        >
          @if(!empty($book))
            <option value="{{ $book->id }}" selected>
              {{ $book->title }}
            </option>
          @endif
        </select>
      </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-4 col-lg-3 col-form-label" for="publisher_id">
        {{ __('Yayınevi') }}
      </label>
      <div class="col-sm-8 col-md-6 col-lg-3">
        <select
          class="form-control input-sm"
          name="publisher_id"
          id="publisher_id"
          data-placeholder="{{ __('Yayınevi seçin') }}"
        >
          @if(!empty($publisher))
            <option value="{{ $publisher->id }}" selected>
              {{ $publisher->title }}
            </option>
          @endif
        </select>
      </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-4 col-lg-3 col-form-label" for="printed_on_start">
        {{ __('Bandrol tarihi') }}
      </label>
      <div class="col-sm-8 col-md-6 col-lg-3">
        <input
          type="date"
          class="form-control input-sm"
          name="printed_on_start"
          id="printed_on_start"
          placeholder="YYYY-AA-GG"
          min="2019-01-01"
          max="{{ now()->format('Y-m-d') }}"
          @if(!empty(request('printed_on_start')))
            value="{{ request('printed_on_start') }}"
          @endif
        >
      </div>
      <div class="col-lg-1">
        <div class="py-3 text-center">
          ile
        </div>
      </div>
      <div class="col-sm-8 col-md-6 col-lg-3">
        <input
          type="date"
          class="form-control input-sm"
          name="printed_on_end"
          id="printed_on_end"
          placeholder="YYYY-AA-GG"
          min="2019-01-01"
          max="{{ now()->format('Y-m-d') }}"
          @if(!empty(request('printed_on_end')))
            value="{{ request('printed_on_end') }}"
          @endif
        >
      </div>
      <div class="col-lg-1">
        <div class="py-3">
          arasında
        </div>
      </div>
    </div>

    <button type="submit" class="btn btn-secondary btn-block">
      {{ __('Ara') }}
    </button>
  </form>
    </div>
  </div>
</div>

<div class="container-fluid">
  <table class="table table-striped table-responsive-xs">
    <thead>
      <tr>
        <th>{{ __('Yazar') }}</th>
        <th>{{ __('Çevirmen') }}</th>
        <th>{{ __('Kitap') }}</th>
        <th>{{ __('Baskı Sayısı') }}</th>
        <th>{{ __('Yayınevi') }}</th>
        <th>{{ __('Tarih') }}</th>
      </tr>
    </thead>
    <tbody>
      @if(is_null($list))
        <tr>
          <td colspan="6" class="text-center text-muted">
            {{ __('Yukarıdan bir filtre seçin') }}
          </td>
        </tr>
      @elseif($list->count() > 0)
        @foreach($list as $item)
          <tr>
            <td>
              @if($item->book->authors->count())
                @foreach($item->book->authors as $author)
                  <a href="{{ route('writer.show', $author->id) }}"
                  >{{ $author->name }}</a>@if(!$loop->last),@endif
                @endforeach
              @endif
            </td>
            <td>
              @if($item->book->translators->count())
                @foreach($item->book->translators as $translator)
                  <a href="{{ route('writer.show', $translator->id) }}"
                  >{{ $translator->name }}</a>@if(!$loop->last),@endif
                @endforeach
              @endif
            </td>
            <td>
              <a href="{{ route('book.show', $item->book->id) }}">
                {{ $item->book->title }}
              </a>
            </td>
            <td>
              {{ $item->print_run }}
            </td>
            <td>
              <a href="{{ route('publisher.show', $item->book->publisher->id) }}">
                {{ $item->book->publisher->title }}
              </a>
            </td>
            <td>
              {{ ba_format_date_fe($item->printed_on) }}
            </td>
          </tr>
        @endforeach
      @else
        <tr>
          <td colspan="6" class="text-center text-muted">
            {{ __('Hiç sonuç bulunamadı') }}
          </td>
        </tr>
      @endif
    </tbody>
  </table>

  @if(!empty($list))
    <div class="text-center">
      <div class="d-inline-block">
        {!! $list->render() !!}
      </div>
    </div>
  @endif
</div>
@endsection
