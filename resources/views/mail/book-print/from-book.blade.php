@component('mail::message')
# @lang('Yeni Baskı')

@lang('Takip ettiğiniz :title isimli yeni bandrol alındı', [
  'title' => $book->title,
])

@component('mail::button', ['url' => route('book.show', $book->id), 'color' => 'red'])
@lang('Detaylar İçin Tıklayın')
@endcomponent

Teşekkürler,<br>
{{ config('app.name') }}
@endcomponent
