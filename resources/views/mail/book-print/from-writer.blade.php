@component('mail::message')
# @lang('Yeni Baskı')

@lang('Takip ettiğiniz :name kişisine ait yeni bandrol alındı', [
  'name' => $writer->name,
]).
<br><br>
@lang('Kitap: :title', [
  'title' => $book->title,
])
<br>
@lang('Baskı sayısı: :print_run', [
  'print_run' => $bookPrint->print_run,
])

@component('mail::button', ['url' => route('writer.show', $writer->id), 'color' => 'red'])
@lang('Detaylar İçin Tıklayın')
@endcomponent

Teşekkürler,<br>
{{ config('app.name') }}
@endcomponent
