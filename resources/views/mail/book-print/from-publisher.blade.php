@component('mail::message')
# @lang('Yeni Baskı')

@lang('Takip ettiğin :title yayınevi yeni bandrol aldı', [
  'title' => $publisher->title,
]).
<br><br>
@lang('Kitap: :title', [
  'title' => $book->title,
])

@component('mail::button', ['url' => route('book.show', $book->id), 'color' => 'red'])
@lang('Detaylar İçin Tıklayın')
@endcomponent

Teşekkürler,<br>
{{ config('app.name') }}
@endcomponent
