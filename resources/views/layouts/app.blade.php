<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $meta->seo->title }}</title>

  <!-- Meta -->
  <meta name="keywords" content="{{ $meta->seo->keywords }}">
  <meta name="description" content="{{ $meta->seo->description }}">
  @if($meta->seo->abstract)
      <meta name="abstract" content="{{ $meta->seo->abstract }}">
  @endif
  @if($meta->seo->newsKeywords)
      <meta name="news_keywords" content="{{ $meta->seo->newsKeywords }}">
  @endif

  @if(!empty($meta->seo->ogDescription))
      <meta property="og:description" content="{{ $meta->seo->ogDescription }}">
  @else
      <meta property="og:description" content="{{ $meta->seo->description }}">
  @endif

  @if(!empty($meta->seo->image))
      <meta property="og:image" content="{{ $meta->seo->image }}">
  @endif

  @if(!empty($meta->seo->imageWidth))
      <meta property="og:image:width" content="{{ $meta->seo->imageWidth }}">
  @endif
  @if(!empty($meta->seo->imageHeight))
      <meta property="og:image:height" content="{{ $meta->seo->imageHeight }}">
  @endif

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{ $meta->seo->title }}">
  <meta property="og:site_name" content="{{ config('app.name') }}">
  <meta property="og:url" content="{{ $meta->seo->url }}">

  @if(!empty($meta->seo->robots))
      <meta name="robots" content="{{ $meta->seo->robots }}">
  @endif

  @if(!empty($meta->seo->canonical))
      <link rel="canonical" content="{{ $meta->seo->canonical }}">
  @else
      <link rel="canonical" content="{{ request()->url() }}">
  @endif

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Favicon -->
  <link rel="icon" type="image/png" href="{{ asset('img/ofissizler-logo.png') }}">
  <link rel="shortcut icon" href="{{ asset('img/ofissizler-logo.png') }}">
</head>

<body class="position-relative pb-5">
  <div id="app">
    <nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm">
      <div class="container">
        <a class="navbar-brand text-danger" href="{{ url('/') }}">
          <img src="{{ asset('img/ofissizler-logo.png') }}" style="height: 40px; width: auto;">
          Bandrol Takip
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif
            @else
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
            @endguest
          </ul>
        </div>
      </div>
    </nav>

    <main class="py-4">
      @yield('content')
    </main>

    <footer class="text-center py-2">
      <a href="https://bagimsizatolye.org/" title="Bağımsız Atölye" target="_blank">Bağımsız Atölye</a> tarafından <a href="https://ofissizler.org/" title="Ofissizler" target="_blank">Ofissizler</a> için yapıldı <a href="https://gitlab.com/bagimsizatolye/bandrol-takip" target="_blank" title="Özgür Yazılım">{ <span style="transform: rotate(180deg); display: inline-block">&copy;</span> }</a> <a href="mailto:ege@bagimsizatolye.org?subject=[Ofissizler Bandrol Takip] Sorun/Öneri Bildirimi">Sorun veya öneri bildir</a>
    </footer>
  </div>

  @if(config('services.matomo.status'))
    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["setDomains", ["{{ config('services.matomo.domains') }}"]]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="{{ config('services.matomo.host') }}";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '{{ config('services.matomo.site_id') }}']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Matomo Code -->
  @endif
</body>

</html>
