@extends('layouts.app')

@section('content')
<div class="container" id="author">
  <div class="row mb-3">
    <div class="col-12 col-md-6 text-center text-md-left">
      <h3>{{ $publisher->title }}</h3>
    </div>

    <div class="col-12 col-md-6 text-center text-md-right">
      <form
        method="post"
        action="{{ route('publisher.update', $publisher->id) }}"
        @if(!$subscribed)
          data-confirm="{{ __('Bu yayınevine ait her yeni baskıda e-posta alacaksınız. Emin misiniz?') }}"
        @endif
      >
        @csrf
        <input type="hidden" name="writer_id" value="{{ $publisher->id }}">
        <input type="hidden" name="_method" value="put">
        @if(!$subscribed)
          <input type="hidden" name="subscribe" value="1">
          <button type="submit" class="btn btn-outline-success">Bildirimleri Aç</button>
        @else
          <input type="hidden" name="subscribe" value="0">
          <button type="submit" class="btn btn-success">Bildirimleri Kapat</button>
        @endif
      </form>
    </div>
  </div>

  @if($publisher->books->count())
    <h4 class="mb-3">{{ __('Yayınlanan Kitaplar') }}</h4>
    <div class="row">
      @foreach($publisher->books as $book)
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <a class="book-item" href="{{ route('book.show', $book->id) }}">
            <h5>{{ $book->title }}</h5>
            <p>
              {{ $book->publisher->title }}
            </p>
          </a>
        </div>
      @endforeach
    </div>
  @endif

  @if($bookPrintList->count())
    <h5 class="mt-5 mb-3">{{ __('Baskılar') }}</h5>

    <table class="table table-sm table-responsive">
      <thead>
        <tr>
          <td>{{ __('Kitap adı') }}</td>
          <td>{{ __('Bandrol tarihi') }}</td>
          <td>{{ __('Baskı sayısı') }}</td>
        </tr>
      </thead>
      <tbody>
        @foreach($bookPrintList as $bookPrint)
          <tr>
            <td class="p-2">
              <a href="{{ route('book.show', $bookPrint->book_id) }}">
                {{ $bookPrint->book_title }}
              </a>
            </td>
            <td class="p-2">{{ ba_format_date_fe($bookPrint->printed_on) }}</td>
            <td class="p-2 text-right">{{ $bookPrint->print_run }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  @endif
</div>
@endsection
