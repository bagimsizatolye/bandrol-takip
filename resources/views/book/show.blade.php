@extends('layouts.app')

@section('content')
<div class="container" id="author">
  <div class="row mb-3">
    <div class="col-12 col-md-6 text-center text-md-left">
      <h3>{{ $book->title }}</h3>
    </div>

    <div class="col-12 col-md-6 text-center text-md-right">
      <form
        method="post"
        action="{{ route('book.update', $book->id) }}"
        @if(!$subscribed)
          data-confirm="{{ __('Bu kitaba ait her yeni baskıda e-posta alacaksınız. Emin misiniz?') }}"
        @endif
      >
        @csrf
        <input type="hidden" name="writer_id" value="{{ $book->id }}">
        <input type="hidden" name="_method" value="put">
        @if(!$subscribed)
          <input type="hidden" name="subscribe" value="1">
          <button type="submit" class="btn btn-outline-success">Bildirimleri Aç</button>
        @else
          <input type="hidden" name="subscribe" value="0">
          <button type="submit" class="btn btn-success">Bildirimleri Kapat</button>
        @endif
      </form>
    </div>
  </div>

  <p>
    <strong>{{ __('Yayıncı:') }}</strong>
    <a href="{{ route('publisher.show', $book->publisher_id) }}">
      {{ $book->publisher->title }}
    </a>
  </p>
  <p>
    <strong>{{ __('Yazar:') }}</strong>
    @foreach($book->authors as $writer)
      <a href="{{ route('writer.show', $writer->id) }}"
      >{{ $writer->name }}</a>@if(!$loop->last),@endif
    @endforeach
  </p>
  @if($book->translators->count())
    <p>
      <strong>{{ __('Çevirmen:') }}</strong>
      @foreach($book->translators as $writer)
        <a href="{{ route('writer.show', $writer->id) }}"
        >{{ $writer->name }}</a>@if(!$loop->last),@endif
      @endforeach
    </p>
  @endif

  @if($book->bookPrints()->count())
    <h5 class="mt-5 mb-3">{{ __('Baskılar') }}</h5>

    <table class="table table-sm table-responsive">
      <thead>
        <tr>
          <td>{{ __('Bandrol tarihi') }}</td>
          <td>{{ __('Baskı sayısı') }}</td>
        </tr>
      </thead>
      <tbody>
        @foreach($book->bookPrints as $bookPrint)
          <tr>
            <td class="p-2">{{ ba_format_date_fe($bookPrint->printed_on) }}</td>
            <td class="p-2 text-right">{{ $bookPrint->print_run }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  @endif
</div>
@endsection
