<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 1000);
            $table->integer('publisher_id')->unsigned();
            $table->integer('book_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('publisher_id')
                ->references('id')->on('publishers')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('book_type_id')
                ->references('id')->on('book_types')
                ->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
