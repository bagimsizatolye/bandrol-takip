<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookWritersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_writers', function (Blueprint $table) {
            $table->integer('book_id')->unsigned();
            $table->integer('writer_id')->unsigned();
            $table->tinyInteger('writer_status')->default(\App\Models\Writer::sAuthor);
            $table->timestamps();

            $table->foreign('book_id')
                ->references('id')->on('books')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->foreign('writer_id')
                ->references('id')->on('writers')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->primary([
                'book_id',
                'writer_id',
                'writer_status',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_writers');
    }
}
