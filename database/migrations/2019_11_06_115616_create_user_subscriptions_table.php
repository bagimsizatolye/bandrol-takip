<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('book_id')->unsigned()->nullable();
            $table->integer('publisher_id')->unsigned()->nullable();
            $table->integer('writer_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('cascade');
            $table->foreign('book_id')
                ->references('id')->on('books')
                ->onUpdate('restrict')->onDelete('cascade');
            $table->foreign('publisher_id')
                ->references('id')->on('publishers')
                ->onUpdate('restrict')->onDelete('cascade');
            $table->foreign('writer_id')
                ->references('id')->on('writers')
                ->onUpdate('restrict')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subscriptions');
    }
}
