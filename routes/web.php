<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@getHome')
    ->name('home');
Route::get('book-print', 'HomeController@bookPrintIndex')
    ->name('book-print.index');

Route::resource('writer', 'WriterController')->only([
    'show',
    'update',
]);
Route::resource('book', 'BookController')->only([
    'show',
    'update',
]);
Route::resource('publisher', 'PublisherController')->only([
    'show',
    'update',
]);
